// 字体大小列表对象
export const FONT_SIZE_LIST = [
  { fontSize: 12 },
  { fontSize: 14 },
  { fontSize: 16 },
  { fontSize: 18 },
  { fontSize: 20 },
  { fontSize: 22 },
  { fontSize: 24 }
]

// 字体列表
export const FONT_FAMILY = [
  { font: 'Default' },
  { font: 'Cabin' },
  { font: 'Days One' },
  { font: 'Montserrat' },
  { font: 'Tangerine' }
]

// 主体列表
export function themeList (vue) {
  return [
    {
      alias: vue.$t('book.themeDefault'),
      name: 'Default',
      style: {
        body: {
          'color': '#4c5059',
          'background': '#cecece'
        }
      }
    },
    {
      alias: vue.$t('book.themeGold'),
      name: 'Gold',
      style: {
        body: {
          'color': '#5c5b56',
          'background': '#c6c2b6'
        }
      }
    },
    {
      alias: vue.$t('book.themeEye'),
      name: 'Eye',
      style: {
        body: {
          'color': '#404c42',
          'background': '#a9c1a9'
        }
      }
    },
    {
      alias: vue.$t('book.themeNight'),
      name: 'Night',
      style: {
        body: {
          'color': '#cecece',
          'background': '#000000'
        }
      }
    }
  ]
}

// 动态添加css
export function addCss (href) {
  // 创建一个link标签
  const link = document.createElement('link')
  // 给link标签添加属性
  link.setAttribute('rel', 'stylesheet')
  link.setAttribute('type', 'text/css')
  link.setAttribute('href', href)
  // 将link标签添加到head标签的后面
  document.getElementsByTagName('head')[0].appendChild(link)
}

// 动态移除css标签
export function removeCss (href) {
  // 获取到所有的link标签
  const links = document.getElementsByTagName('link')
  // 遍历link标签集合
  for (let i = links.length; i >= 0; i--) {
    const link = links[i]
    if (link && link.getAttribute('href') && link.getAttribute('href') === href) {
      link.parentNode.removeChild(link)
    }
  }
}

// 移除所有主题css标签
export function removeAllThemeCss () {
  removeCss(`${process.env.VUE_APP_RES_URL}/theme/theme_Default.css`)
  removeCss(`${process.env.VUE_APP_RES_URL}/theme/theme_Eye.css`)
  removeCss(`${process.env.VUE_APP_RES_URL}/theme/theme_Gold.css`)
  removeCss(`${process.env.VUE_APP_RES_URL}/theme/theme_Night.css`)
}
